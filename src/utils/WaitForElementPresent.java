package utils;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.seleniumhq.jetty9.server.Authentication.Failed;

import com.thoughtworks.selenium.webdriven.commands.IsElementPresent;

public class WaitForElementPresent {

	public boolean isElementPresent(By by) {

		WebDriver driver = new FirefoxDriver();
		try {
			driver.findElement(by);
			driver.wait(6000);

			if (isElementPresent(By.xpath("//a[@class='btn btn-lg btn-success']"))) {
				return true;

			} else {
				return false;

			}
		} catch (NoSuchElementException e) {

			System.out.println(e.getMessage());
			return false;

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			return false;

		}
	}
}
