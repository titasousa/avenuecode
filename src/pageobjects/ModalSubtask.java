package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Calendar;

public class ModalSubtask {

	WebDriver driver;

	/*
	 * Xpath of the elements in modal
	 */
	By editTask = By.id("edit_task");
	By subTaskDescr = By.id("new_sub_task");
	By dueDate = By.id("dueDate");
	By addSubTask = By.id("add-subtask");
	By close = By.xpath("//div/button[@class='btn btn-primary']");

	public ModalSubtask(WebDriver driver) {

		this.driver = driver;
	}

	public void editTask() {
		driver.findElement(editTask).sendKeys("Editing task");
	}

	public void subTaskDescr() {
		driver.findElement(subTaskDescr).sendKeys("SubtaskA");
	}

	public void dueDate() {
		Calendar c = Calendar.getInstance();
		driver.findElement(dueDate).sendKeys("18/06/2017");
	}

	public void addSubtask() {
		driver.findElement(addSubTask).click();
	}

	public void closeModal() {
		driver.findElement(close).click();
	}

	public void checkAddedSubTask() {
		driver.getPageSource().contains("SubTask A");
	}

	public void dueDateChar() {
		driver.findElement(dueDate).sendKeys("abc123");
	}

	public void emptySubTask() {
		driver.getPageSource().contains("emtpy");
	}

	public void more250char() {
		driver.findElement(subTaskDescr).sendKeys(
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum");
	}

}
