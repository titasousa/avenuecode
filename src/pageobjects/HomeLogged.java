package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.thoughtworks.selenium.webdriven.commands.WaitForPageToLoad;

public class HomeLogged {

	WebDriver driver;

	By myTasks = By.xpath("//a[@class='btn btn-lg btn-success']");
	By addTask = By.xpath("//div/span[@class='input-group-addon glyphicon glyphicon-plus']");
	By removeTask = By.xpath("//td/button[@class='btn btn-xs btn-danger']");
	By textTask = By.id("new_task");
	By manageSubtask = By.xpath("//td/button[@class='btn btn-xs btn-primary ng-binding']");

	public HomeLogged(WebDriver driver) {

		this.driver = driver;
	}

	public void myTasks() {
		driver.findElement(myTasks).click();
	}

	public void addTask() {
		driver.findElement(addTask).click();
	}

	public void removeTask() {
		driver.findElement(removeTask).click();
	}

	public void textTask() {
		driver.findElement(textTask).sendKeys("Task A");

	}

	public void emptyTask() {
		driver.getPageSource().contains("empty");
	}

	public void welcomeMessage() {
		driver.getPageSource().contains("here's your todo list today");
	}

	public void less3Char() {
		driver.findElement(textTask).sendKeys("Ts");
	}

	public void more250char() {
		driver.findElement(textTask).sendKeys(
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum");
	}

	public void manageSubtask() {
		driver.findElement(manageSubtask).click();
	}
}
