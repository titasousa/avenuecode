package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class HomePage{
	
	WebDriver  driver;
	
	By signinlnk = By.xpath("//a[@href='/users/sign_in']");
	By email = By.id("user_email");
	By password = By.id("user_password");
	By signinbtn = By.xpath("//input[@class='btn btn-primary']");
	
	public HomePage(WebDriver driver){
		
		this.driver = driver;
	}
	
	public void signinlnk(){
		driver.findElement(signinlnk).click();
	}
	
	public void typeEmail(){
		driver.findElement(email).sendKeys("talitaokt@gmail.com");
	}
	
	public void typePassword(){
		driver.findElement(password).sendKeys("@venu3Talita");
	}
	
	public void signInBtn(){
		driver.findElement(signinbtn).click();
		
	}
}
	

	
	

