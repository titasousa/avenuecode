package tescases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import pageobjects.HomeLogged;


@Test
public class ValidWelcomeMessage {
	
	public void checkWelComeMessage(){
		
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://qa-test.avenuecode.com/");
		
		/*
		 * Login on app
		 */
		LoginAvenue login = new LoginAvenue();
		login.validLogin();
		
		HomeLogged checkMessage = new HomeLogged(driver);
		
		checkMessage.welcomeMessage();
		
	}
	
	
	
	
	
	
	

}
