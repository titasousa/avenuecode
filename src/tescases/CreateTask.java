package tescases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;


import pageobjects.HomeLogged;

@Test
public class CreateTask {
	
	public void createTask(){
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		
		/*
		 * Login on app
		 */
		LoginAvenue login = new LoginAvenue();
		login.validLogin();
		
		HomeLogged myTasks = new HomeLogged(driver);
		
		myTasks.myTasks();
		myTasks.textTask();
		myTasks.addTask();
		
		driver.quit();
				
	}

	
}
