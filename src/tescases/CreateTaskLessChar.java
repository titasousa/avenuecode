package tescases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import pageobjects.HomeLogged;

@Test
public class CreateTaskLessChar {
	
	public void taskLessChar(){
		
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://qa-test.avenuecode.com/");
		
		/*
		 * Login on app
		 */
		LoginAvenue login = new LoginAvenue();
		login.validLogin();
		
		HomeLogged textTask = new HomeLogged(driver);
		
		textTask.less3Char();
		
		System.out.println("System is allowing create task with less of 3 chars");
	}
	

}
