package tescases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import pageobjects.ModalSubtask;
import pageobjects.HomeLogged;

@Test
public class CreateEmptySubTask {

	public void emptySubTask() {
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		// driver.get("http://qa-test.avenuecode.com/");

		/*
		 * Login on app
		 */
		LoginAvenue login = new LoginAvenue();
		login.validLogin();

		HomeLogged manageSubtask = new HomeLogged(driver);

		/*
		 * Instanciate the methods of page objects
		 */

		ModalSubtask addSubTask = new ModalSubtask(driver);
		ModalSubtask emptySubTask = new ModalSubtask(driver);

		manageSubtask.manageSubtask();

		addSubTask.addSubtask();
		emptySubTask.emptySubTask();
		System.out.println("System is allowing create empty subtask");

		driver.quit();

	}

}
