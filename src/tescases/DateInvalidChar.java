package tescases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import pageobjects.ModalSubtask;
import pageobjects.HomeLogged;

@Test
public class DateInvalidChar {
	
	public void dateInvalidChar(){
		
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://qa-test.avenuecode.com/");
		
		
		/*
		 * Login on app
		 */
		LoginAvenue login = new LoginAvenue();
		login.validLogin();

		HomeLogged manageSubtask = new HomeLogged(driver);

		/*
		 * Instanciate the methods of page objects
		 */
		
		
		ModalSubtask subTaskDescr = new ModalSubtask(driver);
		ModalSubtask dueDateChar = new ModalSubtask(driver);
		ModalSubtask addSubTask = new ModalSubtask(driver);
				
		manageSubtask.manageSubtask();

		subTaskDescr.subTaskDescr();
		dueDateChar.dueDateChar();
		System.out.println("System is allowing fill due date with alphabetic char");
		addSubTask.addSubtask();
		
		driver.quit();

		
	}
	
}
