package tescases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import pageobjects.HomePage;

public class LoginAvenue {


	@Test
	public void validLogin(){
		
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://qa-test.avenuecode.com/users/sign_in");
		
		HomePage login = new HomePage(driver);
		
	
		login.typeEmail();
		login.typePassword();
		login.signInBtn();
		
		driver.quit();
		
		
	}
	
}
