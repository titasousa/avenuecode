package tescases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import pageobjects.ModalSubtask;
import pageobjects.HomeLogged;

@Test
public class CreateSubTaskExceedChar {

	public void subTaskExceedChar() {

		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://qa-test.avenuecode.com/");

		/*
		 * Login on app
		 */
		LoginAvenue login = new LoginAvenue();
		login.validLogin();

		HomeLogged manageSubtask = new HomeLogged(driver);

		ModalSubtask more250char = new ModalSubtask(driver);
		ModalSubtask dueDate = new ModalSubtask(driver);
		ModalSubtask addSubTask = new ModalSubtask(driver);

		manageSubtask.manageSubtask();

		more250char.more250char();
		dueDate.dueDate();
		addSubTask.addSubtask();
		System.out.println("System allowed register more than 250 char in subtask ");

		driver.quit();

	}

}
