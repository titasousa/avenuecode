package tescases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import pageobjects.HomeLogged;

@Test
public class CreateEmptyTask {
	
	public void createEmptyTask(){
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://qa-test.avenuecode.com/");
		
		/*
		 * Login on app
		 */
		LoginAvenue login = new LoginAvenue();
		login.validLogin();
		
		HomeLogged createTask = new HomeLogged(driver);
		
		/*
		 * Check the creation of empty task
		 */
		
		createTask.addTask();
		createTask.emptyTask();
		
		System.out.println("System is creating empty task");
		
		driver.quit();
				
	}
	

}
