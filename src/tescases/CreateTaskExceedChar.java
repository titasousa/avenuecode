package tescases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import pageobjects.HomeLogged;

@Test
public class CreateTaskExceedChar {

	public void createTaskExceedChar() {

		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://qa-test.avenuecode.com/");

		/*
		 * Login on app
		 */
		LoginAvenue login = new LoginAvenue();
		login.validLogin();

		HomeLogged more250char = new HomeLogged(driver);
		HomeLogged addTask = new HomeLogged(driver);

		more250char.more250char();
		addTask.addTask();

		System.out.println("System is allowing create task with more than 250 char");

		driver.quit();
	}

}
