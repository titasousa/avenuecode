package tescases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import pageobjects.ModalSubtask;
import pageobjects.HomeLogged;

@Test
public class EditTaskInSubtask {

	public void editTaskInSubtask() {

		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://qa-test.avenuecode.com/");

		/*
		 * Login on app
		 */
		LoginAvenue login = new LoginAvenue();
		login.validLogin();

		HomeLogged manageSubtask = new HomeLogged(driver);

		/*
		 * Instanciate the methods of page objects
		 */
		ModalSubtask editTask = new ModalSubtask(driver);
		ModalSubtask subTaskDescr = new ModalSubtask(driver);
		ModalSubtask dueDate = new ModalSubtask(driver);
		ModalSubtask addSubTask = new ModalSubtask(driver);
		ModalSubtask checkAddedSubTask = new ModalSubtask(driver);

		manageSubtask.manageSubtask();

		editTask.editTask();
		System.out.println("System is allowing to edit the content of task in subtask modal");

		subTaskDescr.subTaskDescr();
		dueDate.dueDate();
		addSubTask.addSubtask();

		/*
		 * / Check if subtask is really added on task
		 */

		checkAddedSubTask.checkAddedSubTask();
		System.out.println("Subtask created");
		
		driver.quit();
	}

}
